package org.green.bean.injection;

import org.green.bean.injection.bean.BeanA;
import org.green.bean.injection.bean.BeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BeanInjectionApplication implements CommandLineRunner {
    @Autowired
    private BeanA beanA_autowired;

    @Autowired
    private BeanB beanB_autowired;

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(BeanInjectionApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        for (int i = 0; i < 5; i++) {
            System.err.println("beanA_autowired: " + beanA_autowired.getInstanceCount());
        }

        System.err.println("----------------------");

        for (int i = 0; i < 5; i++) {
            System.err.println("beanB_autowired: " + beanB_autowired.getInstanceCount());
        }

        BeanA beanA;
        BeanB beanB;

        System.err.println("----------------------");

        // The same instance of BeanA is reused!
        for (int i = 0; i < 5; i++) {
            beanA = (BeanA) applicationContext.getBean("BeanA");
            System.err.println("BeanA: " + beanA.getInstanceCount());
        }

        System.err.println("----------------------");

        // New instance of BeanB is returned everytime!
        for (int i = 0; i < 5; i++) {
            beanB = (BeanB) applicationContext.getBean("BeanB");
            System.err.println("BeanB: " + beanB.getInstanceCount());
        }
    }
}
