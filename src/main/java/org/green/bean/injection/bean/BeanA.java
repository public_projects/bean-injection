package org.green.bean.injection.bean;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component("BeanA")
public class BeanA {

    private AtomicInteger instanceCount = new AtomicInteger();

    public int getInstanceCount() {
        return instanceCount.incrementAndGet();
    }
}
