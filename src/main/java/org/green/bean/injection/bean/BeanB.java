package org.green.bean.injection.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component("BeanB")
@Scope("prototype")
public class BeanB {
    private AtomicInteger instanceCount = new AtomicInteger();

    public int getInstanceCount() {
        return instanceCount.incrementAndGet();
    }
}
